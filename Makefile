.PHONY: doc

develop:
	sudo python setup.py develop

lint:
	pylint tttb

test:
	python3 setup.py test

doc:
	python3 setup.py build_sphinx

test_upload:
	python3 setup.py sdist
	twine upload --repository testpypi dist/*

upload:
	python3 setup.py sdist
	twine upload --repository pypi dist/*
