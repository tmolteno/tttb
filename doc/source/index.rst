
Welcome to Tensor-Train Toolbox's documentation!
================================================

Tensor-Train Toolbox (tttb) is a python implementation of the tensor-train approximation
work of Ivan Oseledetes [1].

Author: Tim Molteno (tim@elec.ac.nz)

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   base_classes
   tensor_train
   utilities

   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

[1] I. V. Oseledets. "Tensor-Train Decomposition" SIAM J. Sci. Comput., 33(5), 2295–2317. https://doi.org/10.1137/090752286

