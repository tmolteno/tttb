import unittest
import numpy as np

from tttb import tensor

class TestTensor(unittest.TestCase):

    def setUp(self):
        pass


    def test_null(self):

        dut = tensor.Tensor([3, 3, 3])
        dut2 = tensor.Tensor([3, 3, 3])
        self.assertAlmostEqual(dut.d, dut2.d)


    def test_1d_array(self):

        dut = tensor.Tensor.from_ndarray(np.array([3, 3, 3]))
        self.assertEqual(dut.d, 1)
        self.assertEqual(dut.shape(), [3])


    def test_2d_array(self):

        dut = tensor.Tensor.from_ndarray(np.array([[3, 3, 3], [3, 2, 1]]))
        self.assertEqual(dut.d, 2)
        self.assertEqual(dut.shape(), [2, 3])

        self.assertEqual(dut[[1, 1]], 2)
        self.assertEqual(dut[[0, 0]], 3)
        self.assertEqual(dut[[0, 1]], 3)  # Make sure rows and columns are in the right order

    def test_3d_array(self):


        dut = tensor.Tensor.from_ndarray(np.random.rand(3, 3, 4))
        self.assertEqual(dut.d, 3)
        self.assertEqual(dut.shape(), [3, 3, 4])
