import unittest
import numpy as np

from tttb import util

class TestMinRank(unittest.TestCase):

    def test_min_rank(self):
        diagonal = np.array([9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
        r = util.min_rank(diagonal, 0.5)
        self.assertEqual(r, 4)

    def test_min_rank_below(self):
        diagonal = np.array([2, 1])
        r = util.min_rank(diagonal, 0.01)
        self.assertEqual(r, 2)

    def test_min_rank_above(self):
        diagonal = np.array([2, 1])
        r = util.min_rank(diagonal, 8)
        self.assertEqual(r, 0)



class TestTruncatedSVD(unittest.TestCase):

    def svd_check(self, a, eps):
        a_norm = np.linalg.norm(a, 'fro')

        u, s, vh = np.linalg.svd(a, full_matrices=False)
        ut, st, vht, r1 = util.truncated_svd(a, eps)

        b = util.reconstruct_svd(ut, st, vht)

        diff = np.linalg.norm((b-a), 'fro')

        self.assertTrue(diff < eps*a_norm)


    def test_small_symmetric(self):
        for i in range(50):
            n = np.random.randint(5, 100)
            a = np.random.randn(n, n) + 1j*np.random.randn(n, n)
            self.svd_check(a, np.random.uniform(0, 1))


    def test_small_asymmetric(self):
        for i in range(50):
            n = np.random.randint(5, 100)
            m = np.random.randint(5, 100)
            a = np.random.randn(n, m)
            self.svd_check(a, np.random.uniform(0, 1))

