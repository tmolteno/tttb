import unittest
import numpy as np

from functools import reduce

from tttb.tensor_train import TensorTrain
from tttb.tensor import Tensor

class TestTensorTrain(unittest.TestCase):

    def setUp(self):
        pass



    def test_2d_array(self):
        n = 34
        m = 42
        t = Tensor.from_ndarray(np.random.rand(n, m))
        print(t)
        dut = TensorTrain.from_tensor(t, eps=1e-4)
        print(dut)
        self.assertEqual(dut.d, 2)
        self.assertEqual(dut.shape(), [n, m])

        # Test indexing
        for _ in range(1000):
             i = np.random.randint(0, n)
             j = np.random.randint(0, m)
             self.assertAlmostEqual(dut[[i, j]], t[i, j])



    def test_3d_array(self):

        m = 42
        n = 34
        o = 13
        t = Tensor.from_ndarray(np.random.rand(m, n, o))
        print(t)
        dut = TensorTrain.from_tensor(t, eps=1e-2)

        self.assertEqual(dut.d, 3)
        self.assertEqual(dut.shape(), [m, n, o])

        print(dut)
        # Test indexing
        for _ in range(1000):
             i = np.random.randint(0, m)
             j = np.random.randint(0, n)
             k = np.random.randint(0, o)
             self.assertAlmostEqual(dut[[i, j, k]], t[i, j, k])

    def test_4d_array(self):

        m = 42
        n = 34
        o = 13
        p = 11
        vs = [np.linspace(-1, 1, m), np.linspace(-1, 1, n), np.linspace(-1, 1, o), np.linspace(-1, 1, p)]
        m1 = reduce(np.multiply.outer, vs)
        t = Tensor.from_ndarray(np.random.rand(m, n, o, p)*1e-2 + m1)
        print(t)
        dut = TensorTrain.from_tensor(t, eps=1e-3)

        self.assertEqual(dut.d, 4)
        self.assertEqual(dut.shape(), [m, n, o, p])

        print(dut)
        for _ in range(1000):
             i = np.random.randint(0, m)
             j = np.random.randint(0, n)
             k = np.random.randint(0, o)
             l = np.random.randint(0, p)
             self.assertAlmostEqual(dut[[i, j, k, l]], t[i, j, k, l], 3)

    def test_low_rank(self):
        m = 42
        n = 34
        o = 13
        p = 11
        vs = [np.linspace(-1, 1, m), np.linspace(-1, 1, n), np.linspace(-1, 1, o), np.linspace(-1, 1, p)]
        m1 = reduce(np.multiply.outer, vs)
        t = Tensor.from_ndarray(np.random.rand(m, n, o, p)*1e-6 + m1)
        print(t)
        dut = TensorTrain.from_tensor(t, eps=5e-6)
        print(dut)

        self.assertTrue(dut.get_storage() < 500)
        self.assertEqual(dut.d, 4)
        self.assertEqual(dut.shape(), [m, n, o, p])

        # Test indexing
        for _ in range(1000):
             i = np.random.randint(0, m)
             j = np.random.randint(0, n)
             k = np.random.randint(0, o)
             l = np.random.randint(0, p)
             self.assertAlmostEqual(dut[[i, j, k, l]], t[i, j, k, l], 5)


    #def test_qtt_vector(self):
        #d = 10
        #eps = 1e-14
        #a = np.arange(2**d)
        #v = tt.reshape(tt.vector(a), [2]*d, eps=eps)
        #r = v.r
        #self.assertEqual(r.dtype, np.int32, 'Vector ranks are not integers, expected int32')
        #self.assertTrue((r[1:-1] == 2).all(), 'This vector ranks should be exactly 2')
        #b = v.full().reshape(-1, order='F')
        #self.assertTrue(np.linalg.norm(a - b) < 10 * 2**d * eps, 'The approximation error is too large')

